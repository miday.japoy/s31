// Contain all task endpoints for our applications


const express = require('express');
const router = express.Router();
const taskControlller = require('../controllers/taskControllers')


router.get('/', (req, res) => {

	taskControlller.getAllTasks().then(resultFromController => res.send(resultFromController))
})

//route for creating task


router.post('/createTask', (req, res) => {

	taskControlller.createTask(req.body).then(resultFromController => res.send(resultFromController))
})

// route for deleting task

router.delete('/deleteTask/:id', (req, res) => {

	taskControlller.deleteTask(req.params.id).then(resultFromController =>
		res.send(resultFromController))
})


// route for updating task

router.put('/updateTask/:id', (req, res) => {

	taskControlller.updateTask(req.params.id, req.body).then(resultFromController =>
		res.send(resultFromController))
})


// Activity Code

router.get('/:id', (req, res) => {

	taskControlller.findTask(req.params.id).then(resultFromController =>
		res.send(resultFromController))
})

router.put('/complete/:id', (req, res) => {

	taskControlller.statusCompleted(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
})

module.exports = router