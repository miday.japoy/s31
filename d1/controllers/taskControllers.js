const Task = require('../model/taskSchema')

module.exports.getAllTasks = () => {

	return Task.find({}).then(result => {
		return result
	})
}


module.exports.createTask = (reqBody) => {

	let newTask = new Task({
		name: reqBody.name
	})

	return newTask.save().then((task, err) => {
		if(err){
			console.log(err)
			return false
		} else {
			return task
		}
	})
}


// Delete task

module.exports.deleteTask = (taskId) => {

	return Task.findIdAndDelete(taskId).then((removedTask, err) => {

		if(err){
			console.log(err)
			return false
		} else {

			return removedTask
		}
	})
}

// update task controller

module.exports.updateTask = (taskId, newContent) => {

	return Task.findById(taskId).then((result, err) => {

		if(err){
			console.log(err)
			return false
		}

		result.name = newContent.name
			return result.save().then((updatedTask, saveErr) => {

				if(saveErr){
					console.log(saveErr)
					return false
				} else {

					return updatedTask
				}
			})
	})
}

// Activity code

module.exports.findTask = (taskId) => {

	return Task.findById(taskId).then((result, err) => {
		return result
	})
}


module.exports.statusCompleted = (taskId, taskCompleted) => {

	return Task.findById(taskId).then((result, err) => {

		if(err){
			console.log(err)
			return false
		}

		result.status = taskCompleted.status
			return result.save().then((updatedStatus, saveErr) => {

				if(saveErr){
					console.log(saveErr)
					return false
				} else {

					return updatedStatus
				}
			})
	})
}	